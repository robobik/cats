from random import choice

from flask import Flask, render_template

app = Flask(__name__)

CAT_IMAGES = (
    "https://purr.objects-us-east-1.dream.io/i/20170101_095813.jpg",
    "https://purr.objects-us-east-1.dream.io/i/montybeach.jpeg",
    "https://purr.objects-us-east-1.dream.io/i/1223.jpg",
    "https://purr.objects-us-east-1.dream.io/i/LIkgf.jpg",
    "https://purr.objects-us-east-1.dream.io/i/img_20160629_130809.jpg",
    "https://purr.objects-us-east-1.dream.io/i/image1-8.jpg",
    "https://purr.objects-us-east-1.dream.io/i/039_-_O62Cnj9.gif",
    "https://purr.objects-us-east-1.dream.io/i/Ms0vm.jpg",
    "https://purr.objects-us-east-1.dream.io/i/img_20160407_213701.jpg",
    "https://purr.objects-us-east-1.dream.io/i/abd-13.gif",
)

@app.route("/")
def index():
    image_url = choice(CAT_IMAGES)
    return render_template("index.html", image_url=image_url)

if __name__ == "__main__":
    app.run("0.0.0.0", 9000)
