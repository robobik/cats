# Sample Python App for Dockerization

A simple Flask app which displays a random image of a cat from `https://aws.random.cat/meow`.

## How to run this application

1. Install Python 3.9 or newer and Pip
2. Install requirements: `pip install -r requirements.txt`
3. Run the application: `python3 src/app.py`

The app listens on port 9000.
